#ifdef CODERATE_1_2
#include "./64800x32400.dvb-s2/constantes.h"
#elif CODERATE_8_9
#include "./64800x7200.dvb-s2/constantes.h"
#elif CODERATE_9_10
#include "./64800x6480.dvb-s2/constantes.h"
#else
#error "Please specify coderate by defining either CODERATE_1_2, CODERATE_1_9 or CODERATE_1_10"
#endif // CODERATE_1_2
