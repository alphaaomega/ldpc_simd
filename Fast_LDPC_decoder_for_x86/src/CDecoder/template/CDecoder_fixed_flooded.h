#ifndef __CDecoder_flooded_
#define __CDecoder_flooded_

#include "../../Constantes/constantes_sse.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "./CDecoder_fixed.h"

class CDecoder_fixed_flooded : public CDecoder_fixed {
protected:
    static const size_t cn_deg_max;
    static const size_t vn_deg_max;

    char *_vn_msgs;
    char *_cn_msgs;
    char *_vn_est;

public:
    CDecoder_fixed_flooded();
    virtual ~CDecoder_fixed_flooded();
    virtual void decode(char  var_nodes[], char Rprime_fix[], int nombre_iterations) = 0;
    virtual void decode(float var_nodes[], char Rprime_fix[], int nombre_iterations);
};

#endif
